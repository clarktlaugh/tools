#!/usr/bin/env python


def get_current_public_ip_address():
   import subprocess
   # get current public IP using OpenDNS
   p = subprocess.Popen(['dig', '+short', 'myip.opendns.com', '@resolver1.opendns.com'], stdout=subprocess.PIPE)
   stdout, stderr = p.communicate()
   return stdout.rstrip()


def update_domain_record(api_token, domain, hostname, current_ip_address):
   import digitalocean
   manager = digitalocean.Manager(token=api_token)
   d = manager.get_domain(domain)
   records = d.get_records()

   print "Looking for A record for '{0}'".format(hostname)
   match = next((x for x in records if x.name == hostname and x.type == "A"), None)
   if not match:
      print "A record for '{0}' not found!".format(hostname)
      return
   else:
      if match.data != current_ip_address:
         print "IP address needs to be updated: {0} -> {1}".format(match.data, current_ip_address)
         match.data = current_ip_address
         match.save()
         print "Update successful"
      else:
         print "Already up-to-date"



def main():
   import sys
   import json
   
   if len(sys.argv) < 2:
      print "Usage: {0} config_file.json".format(sys.argv[0])
      return 1

   config = {}   

   with open(sys.argv[1]) as config_file:
      config = json.load(config_file) 

   current_ip = get_current_public_ip_address()

   print "Current public IP address: {0}".format(current_ip)
   update_domain_record(config["api-token"], config["domain"], config["hostname"], current_ip)



if __name__ == "__main__":
    main()



